$(document).ready(function(){
  var markers = [];
  var local = String;
    var map= new GMaps({
      div: '#map',
      lat: -23.162408,
      lng: -45.795275,
      zoom: 14
    });
  
    $('#evento').submit(function(e){
    local = $('#endereco').val();
    evento = $('#nome_evento').val();
      e.preventDefault();
      GMaps.geocode({
        address: $('#endereco').val().trim(),
        callback: function(result,status){
          if(status=='OK'){
            var latlng = result[0].geometry.location;
            map.setCenter(latlng.lat(),latlng.lng());
            map.addMarker({
              lat: latlng.lat(),
              lng: latlng.lng(),
            title: evento,
            infoWindow: {
              content : '<p>' + evento + '</p>' + '<p>' + local + '</p>',
            }
            });
          }
        }
      });
    });  
});
